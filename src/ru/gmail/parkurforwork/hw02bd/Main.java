package ru.gmail.parkurforwork.hw02bd;

import java.sql.*;
import java.util.Scanner;

public class Main {

    private static Scanner sc;
    private static Connection connection;
    private static Statement statement;
    private static final String createRow = "INSERT INTO Products \n " +
            "(prodid, title, cost) VALUES (?, ?, ?)";
    private static final String createTable = "CREATE TABLE IF NOT EXISTS Products \n" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            "prodid INTEGER UNIQUE, \n" +
            "title TEXT, \n" +
            "cost INTEGER)";
    private static final String clearTable = "DELETE FROM Products";
    private static final String showByName = "SELECT * FROM Products WHERE title = ?";
    private static final String changePriceByName = "UPDATE Products SET cost = ? WHERE title = ?";
    private static final String priceRange = "SELECT * FROM Products WHERE cost >= ? AND cost <= ?";
    PreparedStatement ps;

    public static void main(String[] args) {

        connect();

        createAndFillTable();

        sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            PreparedStatement userAction;
            String[] tokens = sc.nextLine().split(" ");
            switch (tokens[0]) {
                case "/showpricebyname":
                    if (tokens.length == 2) {
                        try {
                            userAction = connection.prepareStatement(showByName);
                            userAction.setString(1, tokens[1]);
                            ResultSet res = userAction.executeQuery();
                            if (res.isBeforeFirst()) {
                                while (res.next()) {
                                    System.out.println("Цена для " + res.getString(3)
                                            + " равна " + res.getInt(4));
                                }
                            } else System.out.println("No result for your request");
                            break;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case "/changepricebyname":
                    try {
                        if (tokens.length == 3) {
                            userAction = connection.prepareStatement(changePriceByName);
                            userAction.setInt(1, Integer.parseInt(tokens[2]));
                            userAction.setString(2, tokens[1]);
                            userAction.executeUpdate();
                            userAction = connection.prepareStatement(showByName);
                            userAction.setString(1, tokens[1]);
                            ResultSet res = userAction.executeQuery();
                            if (res.isBeforeFirst()) {
                                while (res.next()) {
                                    System.out.println("Цена для " + res.getString(3)
                                            + " изменена на " + res.getInt(4));
                                }
                            }
                        } else System.out.println("Incorrect command");
                        break;
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case "/showbypricerange":
                    if (tokens.length == 3) {
                        try {
                            userAction = connection.prepareStatement(priceRange);
                            userAction.setInt(1, Integer.parseInt(tokens[1]));
                            userAction.setInt(2, Integer.parseInt(tokens[2]));
                            ResultSet res = userAction.executeQuery();
                            if (res.isBeforeFirst()) {
                                while (res.next()) {
                                    System.out.println(res.getString(3)
                                            + res.getInt(4));
                                }
                            } else System.out.println("No result for your request");
                            break;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case "/exit":
                    sc.close();
                    break;
                default:
                    System.out.println("Incorrect command");
            }
        }

        disconect();
    }

    public static void createAndFillTable() {
        try {
            statement = connection.createStatement();

            statement.execute(createTable);
            statement.execute(clearTable);

            PreparedStatement ps = connection.prepareStatement(createRow);

            connection.setAutoCommit(false);
            for (int i = 0; i < 10_000; i++) {
                ps.setInt(1, i);
                ps.setString(2, "Товар" + i);
                ps.setInt(3, i * 10);
                ps.addBatch();
            }
            ps.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:hw02db.db");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void disconect() {
        try {
            connection.close();
        } catch (SQLException e) {

        }
    }
}
